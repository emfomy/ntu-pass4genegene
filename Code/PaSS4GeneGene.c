/**
 * Particle Swarm Stepwise (PaSS) Algorithm for Gene-Gene
 * PaSS4GeneGene.c
 * The main functions
 */

/**@mainpage
 * @author Mu Yang
 */

/* 
 * General Model:
 * Y = X * Beta + error
 * R = Y - X * Beta
 *
 * Select index in forward step:
 * idx = argmax_{i not in I} abs( X[i col]' * R  )
 *
 * Select index in backward step:
 * idx = argmin_{i in I} norm( R_{I exclude i} )
 *     = argmin_{i in I} norm( R + Beta[i] * X[i col] )
 *
 *
 * Tensor model:
 * y_i   = vecp( x_i )' * vecp( beta ) + error
 * Y     = X * Beta                    + error
 * R     = Y - X * Beta
 * Y    := [ y_i ]         , i = 1 to n
 * X    := [ vecp( x_i ) ]', i = 1 to n
 * Beta := vecp( beta )
 *
 * Note:
 * X[i cols] := [ x_k[i col] ], k = 1 to n
 *
 * Select index in forward step:
 * idx = argmax_{i not in I} norm( X[i cols]' * R )
 *
 * Select index in backward step:
 * idx = argmin_{i in I} norm( R_{I exclude i} )
 *     = argmin_{i in I} norm( R + X[i cols] * Beta[i col] )
 *
 * Note that X in tensor model is symmetric without main diagonal.
 * Note that vecp denotes the operator that stacks the lower half (excluding diagonals) of a symmetric matrix columnwise into a long vector.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mkl.h>
#include <omp.h>

#define passSeed time( NULL )


/**
 * The criterion enumeration
 */
enum Criterion {
	AIC,   /**< Akaike information criterion */
	BIC,   /**< Bayesian information criterion */
	EBIC,  /**< Extended Bayesian information criterion */
	HDBIC, /**< High-dimensional Bayesian information criterion */
	HDHQ   /**< High-dimensional Hannan-Quinn information criterion */
};


/**
 * The parameter structure
 */
struct Parameter {
	int nP;    /**< the number of particles */
	int nI;    /**< the number of iterations */
	float pfg; /**< the probability for forward step: global */
	float pfl; /**< the probability for forward step: local */
	float pfr; /**< the probability for forward step: random */
	float pbl; /**< the probability for backward step: local */
	float pbr; /**< the probability for backward step: random */
};


/**
 * The data structure
 */
struct Data {
	float *X;       /**< matrix, n by k, the regressors */
	float *Y;       /**< vector, n by 1, the regressand */
	float *Beta;    /**< vector, k by 1, the effects */
	float *Theta;   /**< vector, k by 1, X'*Y */
	float *M;       /**< matrix, k by k, inv( X'*X ), upper general storage */
	float *R;       /**< vector, n by 1, the residual */
	float *B;       /**< vector, n by 1, working vecter */
	float *D;       /**< vector, n by 1, working vecter */
	float e;        /**< scalar, the norm of R */
	float phi;      /**< scalar, the value given by criterion */
	float phi_old;  /**< scalar, the value given by criterion of past iteration */
	float phi_best; /**< scalar, the value given by criterion, best solution */

	int *Idx_lf;    /**< vector, k by 1, map local effects to full effects */
	int *Idx_fl;    /**< vector, p by 1, map full effects to local effects */
	int *I;         /**< vector, q by 1, the chosen indices (boolean) */
	int *I_best;    /**< matrix, q by 1, the chosen indices (boolean), best solution */
	int k;          /**< scalar, the number of chosen effects */
	int l;          /**< scalar, the number of chosen indices */

	int stat;       /**< boolean, the status, 1: forward step, 0: backward step */

	int *Itemp;     /**< vector, q by 1 */
};


/**
 * The report structure
 */
struct Report {
	int nC;    /**< the number of correct */
	int nPS;   /**< the number of positive selection */
	int nFD;   /**< the number of false discovery */
	float CR;  /**< the correct rate */
	float ICR; /**< the incorrect rate */
	float PSR; /**< the positive selection rate */
	float FDR; /**< the false discovery rate */
};


// Global variables
int n;                /**< scalar, the number of statistical units */
int q0;               /**< scalar, the number of general indices */
int q;                /**< scalar, the length of total indices, 2*q0 */
int p;                /**< scalar, the number of total effects, q0*(q0+1)/2 */
float *X;             /**< matrix, n by p, the regressors */
float *Y;             /**< vector, n by 1, the regressand */
int *J;               /**< matrix, q by 1, the chosen indices (boolean), true solution */
int *IdxMap;          /**< matrix, q0 by q0, the menery index of tensor regressors */
float ebic_gamma;     /**< scalar, a penalty parameter in EBIC */
enum Criterion cri;   /**< the criterion */
struct Parameter par; /**< the parameter */
char *dataname;       /**< string, the name of data */
int nT;               /**< the number of tests */
int nthr;             /**< the number of threads */

const float fNaN = 0.0f/0.0f;


// Functions
void pass_cfg( const char* );
void pass_load( const char* );
void pass_main( struct Data* );
void pass_malloc_data( struct Data* );
void pass_free_data( struct Data* );
void pass_init_model( struct Data*, const int );
void pass_select_fb( struct Data* );
void pass_update_model( struct Data*, const int );
void pass_compute_cri( struct Data* );


/**
 * Main function
 */
int main( int argc, char** argv ) {
	int i, j, k, t, *IdxMapTemp, totalCorrect, totalChoose, isize, ione = 1, itemp;
	float ftemp;
	double start_time, total_time = 0.0, compute_time;
	char *cfgroot, *dataroot, *cristr;
	struct Report report;
	struct Data *data, tdata, *bdata;

	/*================================================================*
	 * Set parameters and load data                                   *
	 *================================================================*/

	par.nP  = 32;
	par.nI  = 128;

	par.pfg = 0.1f;
	par.pfl = 0.8f;
	par.pfr = 0.1f;
	par.pbl = 0.9f;
	par.pbr = 0.1f;

	cri = EBIC;
	ebic_gamma = 1.0f;

	nT = 10;
	nthr = 0;

	cfgroot = "PaSS4GeneGene.cfg";
	dataroot = "PaSS4GeneGene.dat";
	dataname = "";

	printf( "\n================================\n\n" );

	if( argc > 1 ) cfgroot  = argv[1];
	if( argc > 2 ) dataroot = argv[2];

	// Load config
	pass_cfg( cfgroot );

	// Check parameter
	if( par.nP <= 0 ) {
		printf( "nP must be positive!\n" );
		exit( 1 );
	}
	if( par.nI <= 0 ) {
		printf( "nI must be positive or zero!\n" );
		exit( 1 );
	}
	if( nT < 0 ) {
		printf( "nT must be positive or zero!\n" );
		exit( 1 );
	}

	// Set criterion
	switch(cri) {
	case AIC: 
		cristr = "AIC";
		break;
	case BIC:
		cristr = "BIC";
		break;
	case EBIC:
		cristr = "EBIC";
		break;
	case HDBIC:
		cristr = "HDBIC";
		break;
	case HDHQ:
		cristr = "HDHQ";
		break;
	}

	// Set threads
	itemp = omp_get_max_threads();
	if( nthr > itemp || nthr <= 0 ) {
		nthr = itemp;
	}
	if( nthr > par.nP ) {
		nthr = par.nP;
	}
	printf( "Uses %d threads.\n", nthr );

	// Initialize Random Seed
	srand( passSeed );

	// Load data
	pass_load( dataroot );
	if( cri == EBIC ) {
		printf( "%s: n=%d, q0=%d, q=%d, p=%d, nP=%d, nI=%d, nT=%d, cri=%s, gamma=%.1f\n\n", dataname, n, q0, q, p, par.nP, par.nI, nT, cristr, ebic_gamma );
	}
	else {
		printf( "%s: n=%d, q0=%d, q=%d, p=%d, nP=%d, nI=%d, nT=%d, cri=%s\n\n", dataname, n, q0, q, p, par.nP, par.nI, nT, cristr );
	}

	/*================================================================*
	 * Centralize and Normalize the original data                     *
	 *================================================================*/

	// Centralize X
	for( i = 0; i < p; i++ ) {
		ftemp = 0.0f;
		for( j = 0; j < n; j++ ) {
			ftemp += X[i*n+j];
		}
		ftemp /= n;
		for( j = 0; j < n; j++ ) {
			X[i*n+j] -= ftemp;
		}
	}

	// Normalize X
	for( i = 0; i < p; i++ ) {
		ftemp = 1.0f / snrm2( &n, X + i*n, &ione );
		sscal( &n, &ftemp, X + i*n, &ione );
	}

	// Centralize Y
	ftemp = 0.0f;
	for( j = 0; j < n; j++ ) {
		ftemp += Y[j];
	}
	ftemp /= n;
	for( j = 0; j < n; j++ ) {
		Y[j] -= ftemp;
	}

	// Normalize Y
	ftemp = 1.0f / snrm2( &n, Y, &ione );
	sscal( &n, &ftemp, Y, &ione );

	/*================================================================*
	 * Initialize the program                                         *
	 *================================================================*/

	// Generate index map
	IdxMapTemp = (int*) malloc( sizeof(int) * q0 * q0 );
	for( i = 1, k = q0; i < q0; i++ ) {
		for( j = 0; j < i; j++, k++ ) {
			IdxMapTemp[i*q0+j] = k;
			IdxMapTemp[j*q0+i] = k;
		}
	}
	for( i = 0; i < q0; i++ ) {
		IdxMapTemp[i*q0+i] = -1;
	}
	IdxMap = IdxMapTemp - 2*p;

	// Allocate memory
	data = (struct Data*) malloc( sizeof(struct Data) * par.nP );
	for( i = 0; i < par.nP; i++ ) {
		pass_malloc_data( data+i );
	}

	// Initialize report
	report.nC = 0;
	report.nPS = 0;
	report.nFD = 0;
	totalCorrect = 0;
	totalChoose = 0;

	// Set output size
	isize = (int) log10( q0 ) + 1;

	/*================================================================*
	 * Create true model                                              *
	 *================================================================*/

	pass_malloc_data( &tdata );

	// Build true model
	pass_init_model( &tdata, q0 );
	for( i = 0; i < q0; i++ ) {
		if( J[i] ) {
			pass_update_model( &tdata, i );
		}
	}
	tdata.stat = 0;
	if( !J[q0] ) {
		pass_update_model( &tdata, q0 );
	}
	if( !J[q0+1] ) {
		pass_update_model( &tdata, q0+1 );
	}
	tdata.stat = 1;
	for( i = q0+2; i < q; i++ ) {
		if( J[i] ) {
			pass_update_model( &tdata, i );
		}
	}
	pass_compute_cri( &tdata );

	// Display model
	printf( "True:\t%12.6f:\t", tdata.phi );
	for( i = 0; i < q0; i++ ) {
		if( tdata.I[i] ) {
			totalCorrect++;
			printf( "g%-*d ", isize, i );
		}
	}
	for( i = q0; i < q; i++ ) {
		if( tdata.I[i] ) {
			totalCorrect++;
			printf( "t%-*d ", isize, i-q0 );
		}
	}
	printf( "\n\n" );

	/*================================================================*
	 * Run PaSS                                                       *
	 *================================================================*/

	for( t = 0; t < nT; t++ ) {
		printf( "%4d:\t", t );

		// Run PaSS
		start_time = omp_get_wtime();
		pass_main( data );
		compute_time = omp_get_wtime() - start_time;
		total_time += compute_time;

		// Find best model
		ftemp = fNaN;
		for( i = 0; i < par.nP; i++ ) {
			if( !(data[i].phi_best > ftemp) ) {
				bdata = data+i;
				ftemp = bdata->phi_best;
			}
		}

		// Build best model
		pass_init_model( &tdata, q0 );
		for( i = 0; i < q0; i++ ) {
			if( bdata->I_best[i] ) {
				pass_update_model( &tdata, i );
			}
		}
		tdata.stat = 0;
		if( !bdata->I_best[q0] ) {
			pass_update_model( &tdata, q0 );
		}
		if( !bdata->I_best[q0+1] ) {
			pass_update_model( &tdata, q0+1 );
		}
		tdata.stat = 1;
		for( i = q0+2; i < q; i++ ) {
			if( bdata->I_best[i] ) {
				pass_update_model( &tdata, i );
			}
		}
		pass_compute_cri( &tdata );

		// Display model
		printf( "%12.6f;\t", tdata.phi );
		for( i = 0; i < q0; i++ ) {
			if( tdata.I[i] ) {
				printf( "g%-*d ", isize, i );
			}
		}
		for( i = q0; i < q; i++ ) {
			if( tdata.I[i] ) {
				printf( "t%-*d ", isize, i-q0 );
			}
		}
		printf( "\n" );

		// Check accuracy
		itemp = 1;
		for( i = 0; i < q; i++ ) {
			if( tdata.I[i] && J[i] ) {
				report.nPS++;
				totalChoose++;
			}
			else if( tdata.I[i] ) {
				itemp = 0;
				report.nFD++;
				totalChoose++;
			}
			else if( J[i] ) {
				itemp = 0;
			}
		}
		report.nC += itemp;
	}

	/*================================================================*
	 * Make report                                                    *
	 *================================================================*/

	// Compute accuracy rate
	totalCorrect *= nT;
	report.CR  = (float) report.nC / nT;
	report.ICR = 1 - report.CR;
	report.PSR = (float) report.nPS / totalCorrect;
	report.FDR = (float) report.nFD / totalChoose;

	// Display report
	printf( "\n================================\n\n" );
	printf( "%s\n", dataname );
	printf( "nP    = %d\n", par.nP );
	printf( "nI    = %d\n", par.nI );
	printf( "pfg   = %.2f\n", par.pfg );
	printf( "pfl   = %.2f\n", par.pfl );
	printf( "pfr   = %.2f\n", par.pfr );
	printf( "pbl   = %.2f\n", par.pbl );
	printf( "pbr   = %.2f\n", par.pbr );
	if( cri == EBIC ) {
		printf( "cri   = EBIC\n" );
		printf( "gamma = %.2f\n", ebic_gamma );
	}
	else {
		printf( "cri   = %s\n", cristr );
	}
	printf( "nT    = %d\n", nT );
	printf( "CR    = %.6f\n", report.CR );
	printf( "ICR   = %.6f\n", report.ICR );
	printf( "PSR   = %.6f\n", report.PSR );
	printf( "FDR   = %.6f\n", report.FDR );
	printf( "Time  = %.6lf sec\n", total_time / nT );
	printf( "\n================================\n\n" );

	/*================================================================*
	 * Free memory                                                    *
	 *================================================================*/

	for( i = 0; i < par.nP; i++ ) {
		pass_free_data( data+i );
	}
	pass_free_data( &tdata );
	free( X );
	free( Y );
	free( J );
	free( data );
	free( IdxMapTemp );

	/*================================================================*/

	return 0;
}


/**
 * Load config from file
 *
 * @param fileroot the root of config file
 */
void pass_cfg( const char* fileroot ) {
	#define BUF_SIZE 1024
	FILE *file;
	int offset1, offset2;
	char line[BUF_SIZE], *cristr;

	printf( "Loading config from '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "r" );

	// Check if file exists
	if( file ) {
		// Read data
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d %d", &par.nP, &par.nI );
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %f %f %f %f %f", &par.pfg, &par.pfl, &par.pfr, &par.pbl, &par.pbr );

		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %n %*s %n", &offset1, &offset2 );
		cristr = (char*) malloc( sizeof(char) * offset2-offset1 );
		sscanf( line, "%*s %s", cristr );
		if( !strcmp( cristr, "AIC" ) ) {
			cri = AIC;
		}
		else if( !strcmp( cristr, "BIC" ) ) {
			cri = BIC;
		}
		else if( !strcmp( cristr, "EBIC" ) ) {
			cri = EBIC;
			ebic_gamma = 1.0f;
		}
		else if( !strcmp( cristr, "HDBIC" ) ) {
			cri = HDBIC;
		}
		else if( !strcmp( cristr, "HDHQ" ) ) {
			cri = HDHQ;
		}
		else if( cristr[0] == 'E' && cristr[1] == 'B' && cristr[2] == 'I' && cristr[3] == 'C' ) {
			cri = EBIC;
			ebic_gamma = atof( cristr+4 );
		}
		else {
			printf( "There is no criterion named '%s'!\n", cristr );
			exit( 1 );
		}

		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d", &nT );
		fgets( line, BUF_SIZE, file );
		sscanf( line, "%*s %d", &nthr );

		// Close file
		fclose( file );

		printf( "Loaded config from '%s'.\n", fileroot );
	}
	else {
		printf( "Unable to open file '%s'!\n", fileroot );

		// Open file
		file = fopen( fileroot, "w" );
		if( !file ) {
			printf( "Unable to create file '%s'!\n", fileroot );
			exit( 1 );
		}
		printf( "Creating config file '%s'...\n", fileroot );

		// Write data
		fprintf( file, "nP/nI %d %d\n", par.nP, par.nI );
		fprintf( file, "prob  %.2f %.2f %.2f %.2f %.2f\n", par.pfg, par.pfl, par.pfr, par.pbl, par.pbr );
		switch(cri) {
		case AIC: 
			fprintf( file, "cri   AIC\n" );
			break;
		case BIC:
			fprintf( file, "cri   BIC\n" );
			break;
		case EBIC:
			fprintf( file, "cri   EBIC%.1f\n", ebic_gamma );
			break;
		case HDBIC:
			fprintf( file, "cri   HDBIC\n" );
			break;
		case HDHQ:
			fprintf( file, "cri   HDHQ\n" );
			break;
		}
		fprintf( file, "nT    %d\n", nT );
		fprintf( file, "nthr  %d\n", nthr );

		fprintf( file, "\n\nNote:\n" );
		fprintf( file, "<nP>:   the number of particles.\n" );
		fprintf( file, "<nI>:   the number of iterations.\n" );
		fprintf( file, "<prob>: <pfg> <pfl> <pfr> <pbl> <pbr>\n" );
		fprintf( file, "<pfg>:  the probability for forward step: global\n" );
		fprintf( file, "<pfl>:  the probability for forward step: local\n" );
		fprintf( file, "<pfr>:  the probability for forward step: random\n" );
		fprintf( file, "<pbl>:  the probability for backward step: local\n" );
		fprintf( file, "<pbr>:  the probability for backward step: random\n" );
		fprintf( file, "<cri>:  the criterion.\n" );
		fprintf( file, "        AIC:         Akaike information criterion.\n" );
		fprintf( file, "        BIC:         Bayesian information criterion.\n" );
		fprintf( file, "        EBIC:        Extended Bayesian information criterion.\n" );
		fprintf( file, "        EBIC<gamma>: EBIC with parameter gamma.\n" );
		fprintf( file, "        HDBIC:       High-dimensional Bayesian information criterion.\n" );
		fprintf( file, "        HDHQ:        High-dimensional Hannan-Quinn information criterion.\n" );
		fprintf( file, "<nT>    the number of tests.\n" );
		fprintf( file, "<nthr>  the number of threads.\n" );
		fprintf( file, "        0: uses system default.\n" );

		// Close file
		fclose( file );

		printf( "Created config file '%s'.\n", fileroot );
		printf( "Uses default config.\n" );
	}
}


/**
 * Load model from file
 *
 * @param fileroot the root of model file
 */
void pass_load( const char* fileroot ) {
	FILE *file;
	int size;

	printf( "Loading model from '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "rb" );
	if( !file ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Read data
	fread( &size, sizeof(int), 1, file );
	dataname = (char*) malloc( sizeof(char) * size );
	fread( dataname, sizeof(char), size, file );
	fread( &n, sizeof(int), 1, file );
	fread( &q0, sizeof(int), 1, file );
	q = 2 * q0;
	p = q0*(q0+1)/2;
	X = (float*) malloc( sizeof(float) * n * p );
	Y = (float*) malloc( sizeof(float) * n );
	J = (int*) malloc( sizeof(int) * q );
	fread( X, sizeof(float), n * p, file );
	fread( Y, sizeof(float), n, file );
	fread( J, sizeof(int), q, file );

	// Close file
	fclose( file );

	printf( "Loaded model from '%s'.\n", fileroot );
}




/**
 * Allocate memory
 *
 * @param data the data
 */
void pass_malloc_data( struct Data* data ) {
	data->X      = (float*) malloc( sizeof(float) * n * n );
	data->Y      = (float*) malloc( sizeof(float) * n );
	data->Beta   = (float*) malloc( sizeof(float) * n );
	data->Theta  = (float*) malloc( sizeof(float) * n );
	data->M      = (float*) malloc( sizeof(float) * n * n );
	data->R      = (float*) malloc( sizeof(float) * n );
	data->B      = (float*) malloc( sizeof(float) * n );
	data->D      = (float*) malloc( sizeof(float) * n );

	data->Idx_lf = (int*) malloc( sizeof(int) * n );
	data->Idx_fl = (int*) malloc( sizeof(int) * p );
	data->I      = (int*) malloc( sizeof(int) * q );
	data->I_best = (int*) malloc( sizeof(int) * q );

	data->Itemp  = (int*) malloc( sizeof(int) * q );
}


/**
 * Free memory
 *
 * @param data the data
 */
void pass_free_data( struct Data* data ) {
	free( data->X );
	free( data->Y );
	free( data->Beta );
	free( data->Theta );
	free( data->M );
	free( data->B );
	free( data->D );
	free( data->R );

	free( data->Idx_lf );
	free( data->Idx_fl );
	free( data->I );
	free( data->I_best );

	free( data->Itemp );
}


/**
 * PaSS main function
 *
 * @param data the data list
 */
void pass_main( struct Data* data ) {
	int tid, i, j, k, l;

	// Use OMP parallel
	#pragma omp parallel private( tid, i, j, k, l ) num_threads( nthr )
	{
		tid = omp_get_thread_num();

		for( j = tid; j < par.nP; j+=nthr ) {
			// Initialize particles
			pass_init_model( data+j, rand() % (p-q0) + q0 );
			pass_compute_cri( data+j );
			data[j].phi_old= data[j].phi;

			// Copy model
			data[j].phi_best = data[j].phi;
			memcpy( data[j].I_best, data[j].I, sizeof(int) * q );
		}

		#pragma omp barrier

		// Find best data
		for( i = 1; i < par.nI; i++ ) {
			for( j = tid; j < par.nP; j+=nthr ) {
				// Update model
				pass_select_fb( data+j );
				pass_compute_cri( data+j );

				if( data[j].phi > data[j].phi_old ) {
					data[j].stat = !data[j].stat;
				}
				if( data[j].k <= 1 ) {
					data[j].stat = 1;
				}
				if( data[j].k + data[j].l >= n-1 || data[j].l >= q-4 ) {
					data[j].stat = 0;
				}

				data[j].phi_old = data[j].phi;

				// Copy model
				for( k = par.nP-2; k < par.nP+2; k++ ) {
					l = (j+k) % par.nP;
					if( data[l].phi_best > data[j].phi ) {
						data[l].phi_best = data[j].phi;
						memcpy( data[l].I_best, data[j].I, sizeof(int) * q );
					}
				}
			}
		}
	}
}


/**
 * Initialize the model
 *
 * @param data the initilizing data
 * @param idx the full index of the effect
 * @note idx must be tensor effect
 */
void pass_init_model( struct Data* data, const int idx ) {
	int i, j, k, ione = 1;

	data->k = 1;
	data->l = 2;

	// Initialize index
	for( i = 0; i < q; i++ ) {
		data->I[i] = 0;
	}

	// Update index
	for( i = 0, k = q0; i < q0; i++, k+=i ) {
		if( k > idx ) {
			break;
		}
		j = idx - k;
	}
	data->I[i+q0] = 1;
	data->I[j+q0] = 1;
	data->Idx_lf[0] = idx;
	data->Idx_fl[idx] = 0;

	// Copy X
	scopy( &n, X + idx*n, &ione, data->X, &ione );

	// Copy Y
	scopy( &n, Y, &ione, data->Y, &ione );

	// M := 1 / (X' * X)
	data->M[0] = 1.0f / sdot( &n, data->X, &ione, data->X, &ione );

	// Theta := X' * Y
	data->Theta[0] = sdot( &n, data->X, &ione, data->Y, &ione );

	// Beta := M * Theta
	data->Beta[0] = data->M[0] * data->Theta[0];

	// Set status
	data->stat = 1;
}


/**
 * Select index to add or remove
 * 
 * @param data the updating data
 */
void pass_select_fb( struct Data* data ) {
	int i, j, idx = -1, choose, ione = 1, itemp;
	float frand = (float)rand() / RAND_MAX, fone = 1.0f, fzero = 0.0f, ftemp, phi_temp = fNaN;

	if( data->stat ) { // Forward step
		// Itemp[0~itemp] := I(best) exclude I(local)
		// Itemp[0~(q-l)] := complement of I(local)
		for( i = 0, j = q-data->l, itemp = 0; i < q; i++ ) {
			if( !(data->I[i]) ) {
				if( data->I_best[i] ) {
					data->Itemp[itemp] = i;
					itemp++;
				}
				else {
					j--;
					data->Itemp[j] = i;
				}
			}
		}

		if( itemp ) {
			choose = ( frand < par.pfg ) + ( frand < par.pfg+par.pfl );
		}
		else {
			choose = frand < par.pfl / ( par.pfl + par.pfr );
		}

		switch( choose ) {
		case 2: // Global best
			idx = data->Itemp[rand() % itemp];
			break;
		case 1: // Local best
			for( i = 0; i < q0; i++ ) { // general part
				if( !(data->I[i]) ) {
					// ftemp := abs( X(full)[i col]' * R )
					ftemp = fabs( sdot( &n, X + i*n, &ione, data->R, &ione ) );

					// Check if this value is maximum
					if( !(ftemp < phi_temp) ) {
						phi_temp = ftemp;
						idx = i;
					}
				}
			}
			for( i = q0; i < q; i++ ) {
				if( !(data->I[i]) ) {
					// B := X(full)[i cols]' * R
					itemp = 0;
					for( j = q0; j < q; j++ ) {
						if( data->I[j] ) {
							data->B[itemp++] = sdot( &n, X + IdxMap[i*q0+j]*n, &ione, data->R, &ione );
						}
					}

					// ftemp = norm( B )
					ftemp = snrm2( &itemp, data->B, &ione );

					// Check if this value is maximum
					if( !(ftemp < phi_temp) ) {
						phi_temp = ftemp;
						idx = i;
					}
				}
			}
			break;
		case 0: // Random
			idx = data->Itemp[rand() % (q-data->l)];
			break;
		}
	}
	else { // Backward step
		if( frand < par.pbl ) { // Local best
			for( i = 0; i < q0; i++ ) { // general part
				if( data->I[i] ) {
					// D := R + Beta[i] * X[i col]
					scopy( &n, data->R, &ione, data->D, &ione );
					itemp = data->Idx_fl[i];
					saxpy( &n, data->Beta + itemp, data->X + itemp*n, &ione, data->D, &ione );

					// ftemp = norm( D )
					ftemp = snrm2( &n, data->D, &ione );

					// Check if this value is minimal
					if( !(ftemp > phi_temp) ) {
						phi_temp = ftemp;
						idx = i;
					}
				}
			}
			for( i = q0; i < q; i++ ) { // tensor part
				if( data->I[i] ) {
					// D := R + X[i cols] * Beta[i col]
					scopy( &n, data->R, &ione, data->D, &ione );
					for( j = q0; j < q; j++ ) {
						if( i != j && data->I[j] ) {
							itemp = data->Idx_fl[IdxMap[i*q0+j]];
							saxpy( &n, data->Beta + itemp, data->X + itemp*n, &ione, data->D, &ione );
						}
					}

					// ftemp = norm( D )
					ftemp = snrm2( &n, data->D, &ione );

					// Check if this value is minimal
					if( !(ftemp > phi_temp) ) {
						phi_temp = ftemp;
						idx = i;
					}
				}
			}
		}
		else { // Random
			idx = -1;
			itemp = rand() % data->l + 1;
			do {
				idx++;
				itemp -= data->I[idx];
			} while( itemp );
		}
	}

	// Update model
	pass_update_model( data, idx );
}


/**
 * Update the model
 *
 * @param data the updating data
 * @param idx the updating full index
 */
void pass_update_model( struct Data* data, const int idx ) {
	int i, j, k, kp, idxE, ione = 1, izero = 0, itemp;
	float *Xnew, a, b, fone = 1.0f, fzero = 0.0f, ftemp;

	if( data->stat ) { // forward step
		if( idx < q0 ) { // general part
			k  = data->k;
			kp = k + 1;
			data->k++;
			data->l++;

			// Update index
			data->Idx_lf[k] = idx;
			data->Idx_fl[idx] = k;

			// Set Xnew
			Xnew = data->X + k*n;

			// Insert new row of X
			scopy( &n, X + idx*n, &ione, Xnew, &ione );

			/*================================================================*
			 * Solve Y = X * Beta to find Beta                                *
			 *================================================================*/

			// B := X' * Xnew
			sgemv( "T", &n, &k, &fone, data->X, &n, Xnew, &ione, &fzero, data->B, &ione );

			// D := M * B
			ssymv( "U", &k, &fone, data->M, &n, data->B, &ione, &fzero, data->D, &ione );

			// a := 1 / (Xnew' * Xnew - B' * D)
			a = 1.0f / ( sdot( &n, Xnew, &ione, Xnew, &ione ) - sdot( &k, data->B, &ione, data->D, &ione ) );

			// insert D with -1.0
			data->D[k] = -1.0f;

			// insert M with zeros
			for( j = 0; j < kp; j++ ) {
				data->M[k*n+j] = 0.0f;
			}

			// M += a * D * D'
			ssyr( "U", &kp, &a, data->D, &ione, data->M, &n );

			// insert Theta with Xnew' * Y
			data->Theta[k] = sdot( &n, Xnew, &ione, data->Y, &ione );

			// insert Beta with zero
			data->Beta[k] = 0.0f;

			// Beta += a * (D' * Theta) * D
			ftemp = a * sdot( &kp, data->D, &ione, data->Theta, &ione );
			saxpy( &kp, &ftemp, data->D, &ione, data->Beta, &ione );

			/*================================================================*/

			// Update index
			data->I[idx] = 1;
		}
		else { // tensor part
			kp = data->k;
			data->l++;

			for( i = q0; i < q; i++ ) {
				if( data->I[i] ) {
					k = kp;
					kp++;

					// Update index
					idxE = IdxMap[idx*q0+i];
					data->Idx_lf[k] = idxE;
					data->Idx_fl[idxE] = k;

					// Set Xnew
					Xnew = data->X + k*n;

					// Insert new row of X
					scopy( &n, X + idxE*n, &ione, Xnew, &ione );

					/*================================================================*
					 * Solve Y = X * Beta to find Beta                                *
					 *================================================================*/

					// B := X' * Xnew
					sgemv( "T", &n, &k, &fone, data->X, &n, Xnew, &ione, &fzero, data->B, &ione);

					// D := M * B
					ssymv( "U", &k, &fone, data->M, &n, data->B, &ione, &fzero, data->D, &ione );

					// a := 1 / (Xnew' * Xnew - B' * D)
					a = 1.0f / ( sdot( &n, Xnew, &ione, Xnew, &ione ) - sdot( &k, data->B, &ione, data->D, &ione ) );

					// insert D with -1
					data->D[k] = -1.0f;

					// insert M with zeros
					for( j = 0; j < kp; j++ ) {
						data->M[k*n+j] = 0.0f;
					}

					// M += a * D * D'
					ssyr( "U", &kp, &a, data->D, &ione, data->M, &n );

					// insert Theta with Xnew' * Y
					data->Theta[k] = sdot( &n, Xnew, &ione, data->
					Y, &ione );

					// insert Beta with zero
					data->Beta[k] = 0.0f;

					// Beta += a * (D' * Theta) * D
					ftemp = a * sdot( &kp, data->D, &ione, data->Theta, &ione );
					saxpy( &kp, &ftemp, data->D, &ione, data->Beta, &ione );

					/*================================================================*/
				}
			}
			data->k = kp;

			// Update index
			data->I[idx] = 1;
		}
	}
	else { // Backward step
		if( idx < q0 ) { // general part
			data->l--;
			data->k--;
			k = data->k;

			// Update index
			data->I[idx] = 0;

			// Find index
			j = data->Idx_fl[idx];

			// Swap j to the end, a := M[end, end], b := Beta[end], D := M[end col]
			if( j != k ) {
				a = data->M[j*n+j];
				b = data->Beta[j];

				scopy( &n, data->X + k*n, &ione, data->X + j*n, &ione );
				data->Beta[j] = data->Beta[k];
				data->Theta[j] = data->Theta[k];
				data->Idx_lf[j] = data->Idx_lf[k];
				data->Idx_fl[data->Idx_lf[j]] = j;

				itemp = k-j-1;
				scopy( &j, data->M + j*n, &ione, data->D, &ione );
				scopy( &itemp, data->M + j*n+n+j, &n, data->D + j+1, &ione );
				data->D[j] = data->M[k*n+j];

				scopy( &j, data->M + k*n, &ione, data->M + j*n, &ione );
				scopy( &itemp, data->M + k*n+j+1, &ione, data->M + j*n+n+j, &n );
				data->M[j*n+j] = data->M[k*n+k];
			}
			else {
				a = data->M[k*n+k];
				b = data->Beta[k];
				scopy( &k, data->M + k*n, &ione, data->D, &ione );
			}

			/*================================================================*
			 * Solve Y = X * Beta to find Beta                                *
			 *================================================================*/

			// M -= 1/a * D * D'
			ftemp = -1.0f/a;
			ssyr( "U", &k, &ftemp, data->D, &ione, data->M, &n );

			// Beta -= b/a * D
			ftemp *= b;
			saxpy( &k, &ftemp, data->D, &ione, data->Beta, &ione );

			/*================================================================*/
		}
		else { // tensor part
			k = data->k;
			data->l--;

			// Update index
			data->I[idx] = 0;

			for( i = q0; i < q; i++ ) {
				if( data->I[i] ) {
					k--;

					// Find index
					j = data->Idx_fl[IdxMap[idx*q0+i]];

					// Swap j to the end, a := M[end, end], b := Beta[end], D := M[end col]
					if( j != k ) {
						a = data->M[j*n+j];
						b = data->Beta[j];

						scopy( &n, data->X + k*n, &ione, data->X + j*n, &ione );
						data->Beta[j] = data->Beta[k];
						data->Theta[j] = data->Theta[k];
						data->Idx_lf[j] = data->Idx_lf[k];
						data->Idx_fl[data->Idx_lf[j]] = j;

						itemp = k-j-1;
						scopy( &j, data->M + j*n, &ione, data->D, &ione );
						scopy( &itemp, data->M + j*n+n+j, &n, data->D + j+1, &ione );
						data->D[j] = data->M[k*n+j];

						scopy( &j, data->M + k*n, &ione, data->M + j*n, &ione );
						scopy( &itemp, data->M + k*n+j+1, &ione, data->M + j*n+n+j, &n );
						data->M[j*n+j] = data->M[k*n+k];
					}
					else {
						a = data->M[k*n+k];
						b = data->Beta[k];
						scopy( &k, data->M + k*n, &ione, data->D, &ione );
					}

					/*================================================================*
					 * Solve Y = X * Beta to find Beta                                *
					 *================================================================*/

					// M -= 1/a * D * D'
					ftemp = -1.0f/a;
					ssyr( "U", &k, &ftemp, data->D, &ione, data->M, &n );

					// Beta -= b/a * D
					ftemp *= b;
					saxpy( &k, &ftemp, data->D, &ione, data->Beta, &ione );

					/*================================================================*/
				}
			}

			data->k = k;
		}
	}
}


/**
 * Compute the value given by criterion
 *
 * @param data the updating data
 */
void pass_compute_cri( struct Data *data ) {
	int ione = 1;
	float fone = 1.0f, fnone = -1.0f;

	// R = Y - X * Beta
	scopy( &n, data->Y, &ione, data->R, &ione );
	sgemv( "N", &n, &data->k, &fnone, data->X, &n, data->Beta, &ione, &fone, data->R, &ione );

	// e := norm(R)
	data->e = snrm2( &n, data->R, &ione );

	// Compute criterion
	switch(cri) {
	case AIC:   // phi := n * log(e^2/n) + 2k
		data->phi = n * (2.0f * log( data->e ) - log( (float)n )) + 2.0f * data->k;
		break;
	case BIC:   // phi := n * log(e^2/n) + k * log(n)
		data->phi = n * (2.0f * log( data->e ) - log( (float)n )) + data->k * log( (float)n );
		break;
	case EBIC:  // phi := n * log(e^2/n) + k * log(n) + 2ebic_gamma * log(p choose k)
		data->phi = n * (2.0f * log( data->e ) - log( (float)n )) + data->k * log( (float)n ) + 2.0f * ebic_gamma * ( lgammaf( p+1.0f ) - lgammaf( p-data->k+1.0f ) - lgammaf( data->k+1.0f ) );
		break;
	case HDBIC: // phi := n * log(e^2/n) + k * log(n) * log(p)
		data->phi = n * (2.0f * log( data->e ) - log( (float)n )) + data->k * log( (float)n ) * log( (float)p );
		break;
	case HDHQ:  // phi := n * log(e^2/n) + 2.01k * log(log(n)) * log(p)
		data->phi = n * (2.0f * log( data->e ) - log( (float)n )) + 2.01f * data->k * log( log( (float)n ) ) * log( (float)p );
		break;
	}
}
