/**
 * Particle Swarm Stepwise (PaSS) Algorithm for Gene-Gene
 * PaSS4GeneGene_Transform_Hung.c
 * Transform data from Hung Hung's data
 */

/**@mainpage
 * @author Mu Yang
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mkl.h>
#include <omp.h>
#include <mat.h>


// Global variables
int n;          /**< scalar, the number of statistical units */
int q0;         /**< scalar, the number of general indices */
int q;          /**< scalar, the number of total indices, 2*q0 */
int p;          /**< scalar, the number of total effects, q0*(q0+1)/2 */
int r;          /**< scalar, the number of using effects */
int qq;         /**< scalar, the number of original indices*/
int k_main;     /**< scalar, the number of true main effects */
int k_int;      /**< scalar, the number of true interactions */
float *X;       /**< matrix, n by p, the regressors */
float *Y;       /**< vector, n by 1, the regressand */
int *J;         /**< matrix, q by 1, the chosen indices (boolean), true solution */
int *X_ind;     /**< vector, q0 by 1, indicate the position in the original position */
int *True_main; /**< vector, k_main by 1, true main effect in the original position */
int *True_int;  /**< vector, 2k_int by 1, true interaction in the original position */
char *dataname; /**< string, the name of data */


// Functions
void gene_load( const char* );
void gene_save( const char* );


/**
 * Main function
 */
int main( int argc, char** argv ) {
	int i, j, k, *IdxTrans;
	char *matroot, *dataroot;

	dataname = "data1_1";
	matroot  = "test_data.mat";
	dataroot = "PaSS4GeneGene.dat";
	qq = 1000;

	if( argc < 2 )
	{
		printf( "Usage: '%s' <dataname>.\n", argv[0] );
		printf( "Usage: '%s' <dataname> <matroot>.\n", argv[0] );
		printf( "Usage: '%s' <dataname> <matroot> <dataroot>.\n", argv[0] );
		printf( "Ex: %s %s %s %s\n", argv[0], dataname, matroot, dataroot );
		exit( 1 );
	}

	dataname = argv[1];
	matroot  = ( argc > 2 ) ? argv[2] : matroot;
	dataroot = ( argc > 3 ) ? argv[3] : dataroot;

	// Load data
	gene_load( matroot );

	// Generate tensor effects of X
	for( i = 1, k = q0; i < q0; i++ ) {
		for( j = 0; j < i; j++, k++ ) {
			vsMul( n, X+i*n, X+j*n, X+k*n );
		}
	}

	// Generate IdxTrans
	IdxTrans = (int*) malloc( sizeof(int) * qq );
	for( i = 0; i < qq; i++ ) {
		IdxTrans[i] = -1;
	}
	for( i = 0; i < q0; i++ ) {
		IdxTrans[X_ind[i]] = i;
	}

	// Change True_main and True_ind
	for( i = 0; i < k_main; i++ ) {
		True_main[i] = IdxTrans[True_main[i]];
	}
	for( i = 0; i < 2*k_int; i++ ) {
		True_int[i] = IdxTrans[True_int[i]];
	}

	// Create J
	for( i = 0; i < k_main; i++ ) {
		if( True_main[i] != -1 ) {
			J[True_main[i]] = 1;
		}
	}
	for( i = 0; i < 2*k_int; i++ ) {
		if( True_int[i] != -1 ) {
			J[True_int[i]+q0] = 1;
		}
	}

	// Save data
	gene_save( dataroot );

	// Clear arrays
	free( X );
	free( Y );
	free( J );
	free( X_ind );
	free( True_main );
	free( True_int );
	free( IdxTrans );

	return 0;
}


/**
 * Load data from mat-file.
 *
 * @param fileroot the root of mat-file
 */
void gene_load( const char* fileroot ) {
	MATFile* matfile;
	mxArray *matData, *matX, *matY, *matX_ind, *matTrue_main, *matTrue_int;
	int i, m;
	double *dtemp;

	printf( "Loading '%s' from '%s'...\n", dataname, fileroot );

	// Open mat-file
	matfile = matOpen( fileroot, "r" );
	if( !matfile ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Open structure data
	matData = matGetVariable( matfile, dataname );
	if( !matData ) {
		printf( "Unable to load data '%s'!\n", dataname );
		exit( 1 );
	}

	// Open matrix X
	matX = mxGetField( matData, 0, "x" );
	if( !matX ) {
		printf( "Unable to load matrix X!\n" );
		exit( 1 );
	}

	// Open array Y
	matY = mxGetField( matData, 0, "y" );
	if( !matY ) {
		printf( "Unable to load array Y!\n" );
		exit( 1 );
	}

	// Open array X_ind
	matX_ind = mxGetField( matData, 0, "x_ind" );
	if( !matX_ind ) {
		printf( "Unable to load array X_ind!\n" );
		exit( 1 );
	}

	// Open array True_main
	matTrue_main = mxGetField( matData, 0, "true_main" );
	if( !matTrue_main ) {
		printf( "Unable to load array True_main!\n" );
		exit( 1 );
	}

	// Open array True_int
	matTrue_int = mxGetField( matData, 0, "true_int" );
	if( !matTrue_int ) {
		printf( "Unable to load array True_int!\n" );
		exit( 1 );
	}

	// Get size
	n = (int) mxGetM( matX );
	q0 = (int) mxGetN( matX );
	k_main = (int) mxGetN( matTrue_main );
	k_int = (int) mxGetN( matTrue_int );
	p = q0*(q0+1)/2;
	q = 2*q0;
	if ( mxGetM( matY ) != n ) {
		printf( "The size of X and Y does not fit!\n" );
		exit( 1 );
	}

	// Allocate memory
	X = (float*) malloc( sizeof(float) * n * p );
	Y = (float*) malloc( sizeof(float) * n );
	J = (int*) calloc( q, sizeof(int) );
	X_ind = (int*) malloc( sizeof(int) * q0 );
	True_main = (int*) malloc( sizeof(int) * k_main );
	True_int = (int*) malloc( sizeof(int) * 2 * k_int );

	// Read data from matrix X
	switch( mxGetClassID ( matX ) ) {
	case mxSINGLE_CLASS:
		memcpy( X, mxGetData( matX ), sizeof(float) * n * q0 );
		break;
	case mxDOUBLE_CLASS:
		printf( "Changing X from double to single...\n" );
		dtemp = (double*) malloc( sizeof(double) * n * q0 );
		memcpy( dtemp, mxGetData( matX ), sizeof(double) * n * q0 );
		for( i = 0; i < n*q0; i++) {
			X[i] = dtemp[i];
		}
		free( dtemp );
		break;
	default:
		printf( "X must be single or double!\n" );
		exit( 1 );
	}

	// Read data from array Y
	switch( mxGetClassID ( matY ) ) {
	case mxSINGLE_CLASS:
		memcpy( Y, mxGetData( matY ), sizeof(float) * n );
		break;
	case mxDOUBLE_CLASS:
		printf( "Changing Y from double to single...\n" );
		dtemp = (double*) malloc( sizeof(double) * n );
		memcpy( dtemp, mxGetData( matY ), sizeof(double) * n );
		for( i = 0; i < n; i++) {
			Y[i] = dtemp[i];
		}
		free( dtemp );
		break;
	default:
		printf( "Y must be single or double!\n" );
		exit( 1 );
	}

	// Read data from array X_ind
	switch( mxGetClassID ( matX_ind ) ) {
	case mxINT32_CLASS:
		memcpy( X_ind, mxGetData( matX_ind ), sizeof(int) * q0 );
		break;
	case mxDOUBLE_CLASS:
		printf( "Changing X_ind from double to integer...\n" );
		dtemp = (double*) malloc( sizeof(double) * q0 );
		memcpy( dtemp, mxGetData( matX_ind ), sizeof(double) * q0 );
		for( i = 0; i < q0; i++) {
			X_ind[i] = dtemp[i];
		}
		free( dtemp );
		break;
	default:
		printf( "X_ind must be integer or double!\n" );
		exit( 1 );
	}

	// Read data from array True_main
	switch( mxGetClassID ( matTrue_main ) ) {
	case mxINT32_CLASS:
		memcpy( True_main, mxGetData( matTrue_main ), sizeof(int) * k_main );
		break;
	case mxDOUBLE_CLASS:
		printf( "Changing True_main from double to integer...\n" );
		dtemp = (double*) malloc( sizeof(double) * k_main );
		memcpy( dtemp, mxGetData( matTrue_main ), sizeof(double) * k_main );
		for( i = 0; i < k_main; i++) {
			True_main[i] = dtemp[i];
		}
		free( dtemp );
		break;
	default:
		printf( "True_main must be integer or double!\n" );
		exit( 1 );
	}

	// Read data from array True_int
	switch( mxGetClassID ( matTrue_int ) ) {
	case mxINT32_CLASS:
		memcpy( True_int, mxGetData( matTrue_int ), sizeof(int) * 2 * k_int );
		break;
	case mxDOUBLE_CLASS:
		printf( "Changing True_int from double to integer...\n" );
		dtemp = (double*) malloc( sizeof(double) * 2 * k_int );
		memcpy( dtemp, mxGetData( matTrue_int ), sizeof(double) * 2 * k_int );
		for( i = 0; i < 2*k_int; i++) {
			True_int[i] = dtemp[i];
		}
		free( dtemp );
		break;
	default:
		printf( "True_int must be integer or double!\n" );
		exit( 1 );
	}

	// Destroy mxArray
	mxDestroyArray( matX );
	mxDestroyArray( matY );
	mxDestroyArray( matX_ind );
	mxDestroyArray( matTrue_main );
	mxDestroyArray( matTrue_int );

	// Close matFile
	matClose( matfile );

	printf( "Loaded '%s' from '%s'.\n", dataname, fileroot );
}


/**
 * Save data into file.
 *
 * @param fileroot the root of model file
 */
void gene_save( const char* fileroot ) {
	FILE *file;
	int size;
	char newdataname[64];

	printf( "Saving model into '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "wb" );
	if( !file ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Set full name
	size = snprintf( newdataname, 64, "Hung_%s", dataname ) + 1;

	// Write data
	fwrite( &size, sizeof(int), 1, file );
	fwrite( newdataname, sizeof(char), size, file );
	fwrite( &n, sizeof(int), 1, file );
	fwrite( &q0, sizeof(int), 1, file );
	fwrite( X, sizeof(float), n * p, file );
	fwrite( Y, sizeof(float), n, file );
	fwrite( J, sizeof(int), q, file );

	// Close file
	fclose( file );

	printf( "Saved model into '%s'.\n", fileroot );
}
