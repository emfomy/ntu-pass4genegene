/**
 * Particle Swarm Stepwise (PaSS) Algorithm for Gene-Gene
 * PaSS4GeneGene_Create_data2.c
 * Create the second data of Gene-Gene
 */

/**@mainpage
 * @author Mu Yang
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mkl.h>
#include <omp.h>

#define passSeed time( NULL )


// Global variables
int n;          /**< scalar, the number of statistical units */
int q0;         /**< scalar, the number of general indices */
int q;          /**< scalar, the number of total indices, 2*q0 */
int p;          /**< scalar, the number of total effects, q0*(q0+1)/2 */
int r0;         /**< scalar, the number of using indices */
int r;          /**< scalar, the number of total using effects, r0*(r0-1)/2 */
float *X;       /**< matrix, n by p, the regressors */
float *Y;       /**< vector, n by 1, the regressand */
int *J;         /**< matrix, q by 1, the chosen indices (boolean), true solution */
char *dataname; /**< string, the name of data */


// Functions
void gene_save( const char* );


/**
 * Main function
 */
int main( int argc, char** argv ) {
	int i, j, k, iseed[4] = { passSeed, passSeed, passSeed, 1 }, ione = 1, ithree = 3, itemp;
	float *Sign, *Beta, fone = 1.0, ftwo = 2.0;
	char *dataroot;

	dataroot = "PaSS4GeneGene.dat";
	dataname = "GeneGene_data2";

	n  = 400;
	q0 = 40;
	p  = q0*(q0+1)/2;
	q  = 2 * q0;
	r0 = 8;
	r  = r0*(r0-1)/2;

	printf( "Creating the second data of Gene-Gene...\n" );

	// Allocate memory
	X    = (float*) malloc( sizeof(float) * n * p );
	Y    = (float*) malloc( sizeof(float) * n );
	J    = (int*)   calloc( q, sizeof(int) );
	Sign = (float*) malloc( sizeof(float) * r );
	Beta = (float*) malloc( sizeof(float) * r );

	// Generate general effects of X with P(X=0) = P(X=2) = 0.25 and P(X=1) = 0.5
	itemp = n*q0;
	slarnv( &ione, iseed, &itemp, X );
	sscal( &itemp, &ftwo, X, &ione );
	vsRound( itemp, X, X );

	// Generate tensor effects of X
	for( i = 1, k = q0; i < q0; i++ ) {
		for( j = 0; j < i; j++, k++ ) {
			vsMul( n, X+i*n, X+j*n, X+k*n );
		}
	}

	// Generate Y using normal random
	slarnv( &ithree, iseed, &n, Y );

	// Generate Sign using with P(Sign=-1) = 0.1 and P(Sign=1) = 0.9
	// Generate Beta using uniform distribution with boundary (0.5, 1)
	// Beta *= Sign
	slarnv( &ione, iseed, &r, Sign );
	slarnv( &ione, iseed, &r, Beta );
	for( i = 0; i < r; i++ ) {
		Beta[i] = Beta[i] * 0.5 + 0.5;
		if( Sign[i] < 0.1 ) {
			Beta[i] = -Beta[i];
		}
	}

	// Y += X[q0~(q0+r) cols] * Beta
	sgemv( "N", &n, &r, &fone, X+q0*n, &n, Beta, &ione, &fone, Y, &ione );

	// Generate J
	for( i = 0; i < r0; i++ ) {
		J[i+q0] = 1;
	}

	// Save data
	gene_save( dataroot );

	// Free memory
	free( X );
	free( Y );
	free( J );
	free( Sign );
	free( Beta );

	return 0;
}


/**
 * Save data into file.
 *
 * @param fileroot the root of model file
 */
void gene_save( const char* fileroot ) {
	FILE *file;
	int size = strlen( dataname ) + 1;

	printf( "Saving model into '%s'...\n", fileroot );

	// Open file
	file = fopen( fileroot, "wb" );
	if( !file ) {
		printf( "Unable to open file '%s'!\n", fileroot );
		exit( 1 );
	}

	// Write data
	fwrite( &size, sizeof(int), 1, file );
	fwrite( dataname, sizeof(char), size, file );
	fwrite( &n, sizeof(int), 1, file );
	fwrite( &q0, sizeof(int), 1, file );
	fwrite( X, sizeof(float), n * p, file );
	fwrite( Y, sizeof(float), n, file );
	fwrite( J, sizeof(int), q, file );

	// Close file
	fclose( file );

	printf( "Saved model into '%s'.\n", fileroot );
}
